package parser;

import org.json.JSONArray;
import org.json.JSONObject;
import pojo.Connection;
import pojo.Item;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Parser {

    public Item parseItem(String jsonStr) {
        try {
            JSONObject obj = new JSONObject(jsonStr);
            Item item = new Item();

            item.id = obj.optString("asin");
            item.title = obj.optString("title");
            item.price = obj.optBigDecimal("price", BigDecimal.ZERO);
            item.categories = getCategories(obj);
            item.ranks = getRanks(obj);
            item.related = getRelated(obj);

            return  item;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Set<Connection> getRelated(JSONObject obj) {
        JSONObject related = obj.optJSONObject("related");
        Set<Connection> connectionSet = new HashSet<>();
        if (related == null) {
            return  connectionSet;
        }
        Map<String, Connection> connections = new HashMap<>();

        JSONArray alsoBought = related.optJSONArray("also_bought");
        updateMap(alsoBought, connections, con -> con.alsoBought = true);

        JSONArray alsoViewed = related.optJSONArray("also_viewed");
        updateMap(alsoViewed, connections, con -> con.alsoViewed = true);

        JSONArray boughtTogether = related.optJSONArray("bought_together");
        updateMap(boughtTogether, connections, con -> con.boughtTogether = true);

        JSONArray buyAfterViewing = related.optJSONArray("buy_after_viewing");
        updateMap(buyAfterViewing, connections, con -> con.buyAfterViewing = true);

        connectionSet.addAll(connections.values());
        return connectionSet;
    }

    private Map<String, BigInteger> getRanks(JSONObject obj) {
        Map<String,BigInteger> result = new HashMap<>();
        JSONObject sales = obj.optJSONObject("salesRank");
        if (sales != null) {

            if (obj != null) {
                Set<String> set = obj.keySet();
                if (set != null) {
                    set.stream()
                            .filter(Objects::nonNull)
                            .forEach(el -> {
                        result.put(el, sales.optBigInteger(el, BigInteger.ZERO));
                    });
                }
            }
        }
        return result;
    }

    private List<List<String>> getCategories(JSONObject obj) {
        List<List<String>> parsed = new LinkedList<>();
        JSONArray categories = obj.getJSONArray("categories");
        for (int i = 0; i < categories.length(); i++) {
            parsed.add(categories.getJSONArray(i).toList().stream().map(String.class::cast).collect(Collectors.toList()));
        }
        return parsed;
    }

    private static void updateMap(JSONArray alsoViewed, Map<String, Connection> connections, Consumer<Connection> marker) {
        if (Objects.nonNull(alsoViewed)) {
            alsoViewed.toList().stream()
                    .map(String.class::cast)
                    .map(str -> Optional.ofNullable(connections.get(str)).orElseGet(() -> new Connection(str)))
                    .peek(marker)
                    .forEach(con -> connections.put(con.target, con));
        }
    }
}
