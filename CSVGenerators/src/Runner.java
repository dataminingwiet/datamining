import instruments.GephiCSVGenerator;
import parser.Parser;

import java.io.IOException;

public class Runner {
    private final static String INPUT_FILE_PATH = "/home/robert/AGHStudia/ED/Metadata/meta_Musical_Instruments.json";
    private final static String INPUT_FILE_PATH_SHORT = "/home/robert/AGHStudia/ED/Metadata/short.json";
    private final static String OUTPUT_FOLDER = "instruments_almost_raw";

    public static void main(String[] args) throws IOException {
        new GephiCSVGenerator().run(INPUT_FILE_PATH, OUTPUT_FOLDER);
    }
}
