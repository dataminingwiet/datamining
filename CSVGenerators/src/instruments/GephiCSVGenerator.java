package instruments;


import csv.CSVItemGenerator;
import csv.CategorizedItemsGraphGenerator;
import csv.CategoryAggregationGraphGenerator;
import parser.Parser;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class GephiCSVGenerator {

    private PrintWriter nodesWriter;
    private PrintWriter edgeWriter;
    private Parser parser = new Parser();

    public void run(String inputPath, String outputfolder) throws IOException {
        new File(outputfolder).mkdirs();

        File nodesFile = new File(outputfolder + "/nodes.csv");
        File edgesFile = new File(outputfolder + "/edges.csv");

        nodesWriter = new PrintWriter(nodesFile);
        edgeWriter = new PrintWriter(edgesFile);

        CSVItemGenerator simpleGenerator = new CSVItemGenerator();
//        CategoryAggregationGraphGenerator simpleGenerator = new CategoryAggregationGraphGenerator();
        nodesWriter.println(simpleGenerator.getNodesHeader());
        edgeWriter.println(simpleGenerator.getEdgeHeader());
//        Files.lines(Paths.get(inputPath))
//                .map(parser::parseItem)
//                .filter(Objects::nonNull)
//                .forEach(simpleGenerator.generateNodes(nodesWriter));
//        Files.lines(Paths.get(inputPath))
//                .map(parser::parseItem)
//                .filter(Objects::nonNull)
//                .forEach(simpleGenerator.generateEdges(edgeWriter));

                Files.lines(Paths.get(inputPath))
                .map(parser::parseItem)
                .filter(Objects::nonNull)
                .forEach(simpleGenerator.consumeItem(nodesWriter, edgeWriter));

//        simpleGenerator.onProcessed(nodesWriter, edgeWriter);

        nodesWriter.flush();
        edgeWriter.flush();
    }
}
