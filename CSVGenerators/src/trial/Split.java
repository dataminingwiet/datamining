package trial;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Split {
    private final static String PATH = "/home/robert/AGHStudia/ED/Separate";
    private final static String OUTPUT_PATH = "/home/robert/AGHStudia/ED/Data";

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        System.setOut(new PrintStream(new FileOutputStream("splt_log_file.txt")));
        System.setErr(new PrintStream(new FileOutputStream("split_err_file.txt")));
        Stream<Path> paths = Files.list(Paths.get(PATH));
        paths.forEach(path -> split(path, new AtomicInteger()));
    }

    private static void split(Path path, AtomicInteger atomicInteger) {
        try {
            long size = Files.size(path);
            String pathStr = path.getFileName().toString();
            pathStr = pathStr.substring(0, pathStr.length() - 5);

            PrintWriter[] outWriters = new PrintWriter[10];
//            for (int i = 0; i < 10; i++) {
//                outWriters[i] = new PrintWriter(OUTPUT_PATH + "/" + pathStr + i + ".json");
//                PrintWriter out = outWriters[atomicInteger.getAndIncrement() % 10];
//                out.println(line);
//            });

            for (int i = 0; i < 10; i++) {
                outWriters[i].close();
            }

        } catch (IOException e) {

        }
    }
}
