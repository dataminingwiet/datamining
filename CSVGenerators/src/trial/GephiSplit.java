package trial;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;

public class GephiSplit {

    private final static String NODES_PATH = "nodes.csv";
    private final static String EDGES_PATH = "edges.csv";

    private static Set<String> names = new HashSet<>();

    static PrintWriter nodesWriter;
    static PrintWriter edgeWriter;

    public static void main(String[] args) throws IOException {
        nodesWriter = new PrintWriter("nodes_split.csv");
        edgeWriter = new PrintWriter("edges_split.csv");
        Files.lines(Paths.get(NODES_PATH)).limit(10_000).forEach(consumeNodeLine());
        nodesWriter.flush();
        Files.lines(Paths.get(EDGES_PATH)).forEach(consumeEdgeLine());

    }

    private static Consumer<? super String> consumeEdgeLine() {
        return edge -> {
            String[] arr = edge.split(",");
            if (names.contains(arr[0]) && names.contains(arr[1])) {
                boolean a =Boolean.parseBoolean(arr[2]);
                boolean b =Boolean.parseBoolean(arr[3]);
                boolean c =Boolean.parseBoolean(arr[4]);
                boolean d =Boolean.parseBoolean(arr[5]);
                edgeWriter.println(new StringJoiner(",")
                        .add(edge)
                        .add("" + (a && b))
                        .add("" + (a && c))
                        .add("" + (a && d))
                        .add("" + (b && c))
                        .add("" + (b && d))
                        .add("" + (c && d))
                        .add("" + (a && b && d)));
                edgeWriter.flush();
            }
        };
    }

    private static Consumer<? super String> consumeNodeLine() {
        return result -> {
            names.add(result.split(",")[0]);
            nodesWriter.println(result);
        };
    }


//    private static void saveEdges(JSONObject obj, String id) {
//        Map<String, Connection> connections = new HashMap<>();
//
//        try {
//            JSONArray alsoBought = obj.getJSONObject("related").getJSONArray("also_bought");
//            updateMap(alsoBought, connections, con -> con.alsoBought = true);
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//        try {
//            JSONArray alsoViewed = obj.getJSONObject("related").getJSONArray("also_viewed");
//            updateMap(alsoViewed, connections, con -> con.alsoViewed = true);
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//        try {
//            JSONArray boughtTogether = obj.getJSONObject("related").getJSONArray("bought_together");
//            updateMap(boughtTogether, connections, con -> con.boughtTogether = true);
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//        try {
//            JSONArray buyAfterViewing = obj.getJSONObject("related").getJSONArray("buy_after_viewing");
//            updateMap(buyAfterViewing, connections, con -> con.buyAfterViewing = true);
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//
//        connections.values().forEach(con -> {
//            edgeWriter.println(new StringJoiner(",").add(id).add(con.target)
//                    .add("" + con.alsoViewed)
//                    .add("" + con.alsoBought)
//                    .add("" + con.boughtTogether)
//                    .add("" + con.buyAfterViewing).toString());
//        });
//
//    }
//
//    private static void updateMap(JSONArray alsoViewed, Map<String, Connection> connections, Consumer<Connection> marker) {
//        alsoViewed.toList().stream()
//                .map(String.class::cast)
//                .map(str -> Optional.ofNullable(connections.get(str)).orElseGet(() -> new Connection(str)))
//                .peek(marker)
//                .forEach(con -> connections.put(con.target, con));
//    }
//
//    private static class Connection {
//        public String target;
//        public boolean alsoBought;
//        public boolean alsoViewed;
//        public boolean boughtTogether;
//        public boolean buyAfterViewing;
//
//        public Connection(String target) {
//            this.target = target;
//        }
//
//        @Override
//        public int hashCode() {
//            return target.hashCode();
//        }
//
//        public boolean equals(Object o) {
//            return this.target.equals(o);
//        }
//    }
//
//    private static void saveNode(JSONObject obj, String id) {
//        String title = null;
//        try {
//            title = obj.getString("title");
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//        BigDecimal price = null;
//        try {
//            price = obj.getBigDecimal("price");
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//        BigInteger musicRank = null;
//        try {
//            musicRank = obj.getJSONObject("salesRank").getBigInteger("Music");
//        } catch (Exception e) {
////            e.printStackTrace();
//        }
//
//        String val = new StringJoiner(",").add(safeStr(id)).add(decorated(title)).add(safeNum(price)).add(safeNum(musicRank)).toString();
//        nodesWriter.println(val);
//    }
//
//    private static String safeStr(Object obj) {
//        if (Objects.isNull(obj)) {
//            return "";
//        }
//        return obj.toString();
//    }
//    private static String safeNum(Object obj) {
//        if (Objects.isNull(obj)) {
//            return "0";
//        }
//        return obj.toString();
//    }
//
//    private static String decorated(Object obj) {
//        return '\"' + safeStr(obj).replaceAll("\"", "\"\"") + '\"';
//    }
}
