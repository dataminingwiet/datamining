package trial;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;

public class Gephi {

    private final static String PATH = "/home/robert/AGHStudia/ED/Metadata/meta_Digital_Music.json";
//    private final static String NODES = "";
//    private final static String EDGES = "";
    static PrintWriter nodesWriter;
    static PrintWriter edgeWriter;
    static PrintWriter categoriesWriter;

    public static void main(String[] args) throws IOException {
        nodesWriter = new PrintWriter("nodes.csv");
        edgeWriter = new PrintWriter("edges.csv");
        categoriesWriter = new PrintWriter("categories.csv");
//        consumeJSON().accept(
//"{'asin': 'B000000261', 'title': 'Return Engagement', 'price': 9.39, '" +
//        "imUrl': 'http://ecx.images-amazon.com/images/I/61FbKsowExL._SY300_.jpg', 'related': {'also_viewed" +
//        "': ['B00189X96O', 'B000005YVA', 'B000001DFV', 'B00008DKB3', 'B00005V62W', 'B00006FIAL', " +
//        "'B000000239', 'B00000028M', 'B000OQF6SQ', 'B000000B2F', 'B000001DH2', 'B003HBGXHS', '" +
//        "B000000241', 'B000000F0R', 'B00000024J', 'B00000024E', 'B008B0EW00', 'B000000EYE', '" +
//        "B00000023B', 'B00005ABLS'], 'buy_after_viewing': ['B00189X96O', 'B000000EYE', 'B0000A1HTX" +
//        "']}, 'salesRank': {'Music': 563832}, 'categories': [['CDs & Vinyl" +
//        "', 'Country', 'Bluegrass'], ['CDs & Vinyl', 'Country', 'Classic Country'], ['CDs & V" +
//        "inyl', 'Country', \"Today's Country\"], ['CDs & Vinyl', 'Folk', 'Contemporary Folk'], ['CDs & Vinyl', " +
//        "'Folk', 'Traditional Folk'], ['CDs & Vinyl', 'Pop'], ['Digital Music', 'Country', 'Bluegrass']]}");
        Files.lines(Paths.get(PATH)).forEach(consumeJSON());
    }

    private static Consumer<? super String> consumeJSON() {
        return  jsonStr -> {
            JSONObject obj = new JSONObject(jsonStr);
            String id = obj.getString("asin");
            saveNode(obj, id);
            saveEdges(obj, id);
//            saveCategories(obj);

            nodesWriter.flush();
            edgeWriter.flush();
        };
    }

    private static void saveEdges(JSONObject obj, String id) {
        Map<String, Connection> connections = new HashMap<>();

        try {
            JSONArray alsoBought = obj.getJSONObject("related").getJSONArray("also_bought");
            updateMap(alsoBought, connections, con -> con.alsoBought = true);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        try {
            JSONArray alsoViewed = obj.getJSONObject("related").getJSONArray("also_viewed");
            updateMap(alsoViewed, connections, con -> con.alsoViewed = true);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        try {
            JSONArray boughtTogether = obj.getJSONObject("related").getJSONArray("bought_together");
            updateMap(boughtTogether, connections, con -> con.boughtTogether = true);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        try {
            JSONArray buyAfterViewing = obj.getJSONObject("related").getJSONArray("buy_after_viewing");
            updateMap(buyAfterViewing, connections, con -> con.buyAfterViewing = true);
        } catch (Exception e) {
//            e.printStackTrace();
        }


        connections.values().forEach(con -> {
            edgeWriter.println(new StringJoiner(",").add(id).add(con.target)
                    .add("" + con.alsoViewed)
                    .add("" + con.alsoBought)
                    .add("" + con.boughtTogether)
                    .add("" + con.buyAfterViewing).toString());
        });

    }

    private static void updateMap(JSONArray alsoViewed, Map<String, Connection> connections, Consumer<Connection> marker) {
        alsoViewed.toList().stream()
                .map(String.class::cast)
                .map(str -> Optional.ofNullable(connections.get(str)).orElseGet(() -> new Connection(str)))
                .peek(marker)
                .forEach(con -> connections.put(con.target, con));
    }

    private static class Connection {
        public String target;
        public boolean alsoBought;
        public boolean alsoViewed;
        public boolean boughtTogether;
        public boolean buyAfterViewing;

        public Connection(String target) {
            this.target = target;
        }

        @Override
        public int hashCode() {
            return target.hashCode();
        }

        public boolean equals(Object o) {
            return this.target.equals(o);
        }
    }

    private static void saveNode(JSONObject obj, String id) {
        String title = null;
        try {
            title = obj.getString("title");
        } catch (Exception e) {
//            e.printStackTrace();
        }

        BigDecimal price = null;
        try {
            price = obj.getBigDecimal("price");
        } catch (Exception e) {
//            e.printStackTrace();
        }

        BigInteger musicRank = null;
        try {
            musicRank = obj.getJSONObject("salesRank").getBigInteger("Music");
        } catch (Exception e) {
//            e.printStackTrace();
        }

        String val = new StringJoiner(",").add(safeStr(id)).add(decorated(title)).add(safeNum(price)).add(safeNum(musicRank)).toString();
        nodesWriter.println(val);
    }

    private static String safeStr(Object obj) {
        if (Objects.isNull(obj)) {
            return "";
        }
        return obj.toString();
    }
    private static String safeNum(Object obj) {
        if (Objects.isNull(obj)) {
            return "0";
        }
        return obj.toString();
    }

    private static String decorated(Object obj) {
        return '\"' + safeStr(obj).replaceAll("\"", "\"\"") + '\"';
    }
}
