package trial;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Main {
    private final static String PATH = "/home/robert/AGHStudia/ED/Data2";
    private final static String OUTPUT_PATH = "/home/robert/AGHStudia/ED/Uploaded2";
    private static Map<String, AtomicInteger> indexers = new HashMap<>();
//    private final static AtomicInteger INDEX = new AtomicInteger();
//    private final static ForkJoinPool closingPool = new ForkJoinPool(4);

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        System.setOut(new PrintStream(new FileOutputStream("log_file.txt")));
        System.setErr(new PrintStream(new FileOutputStream("err_file.txt")));
        getFiles().stream().forEach(path -> {
            System.out.println(String.format("Processing %s...", path));
            try {
                pushToDb(path);
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("WYWALILO SIE NA: " + path.toString());
            }
            System.out.println(String.format("Processing %s done.", path));
        });
    }

    private static void pushToDb(Path path) throws ExecutionException, InterruptedException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(32);
        forkJoinPool.submit(() -> {
                    //parallel task here, for example
                    try {
                        String pathStr= path.getFileName().toString().toLowerCase();
                        String prefix = pathStr.substring(0, pathStr.length() -7);
                        AtomicInteger index = Optional.ofNullable(indexers.get(prefix)).orElseGet(() ->  {
                            indexers.put(prefix, new AtomicInteger());
                            return indexers.get(prefix);
                        });
                        Files.lines(path).parallel().forEach(pushObj2Db(pathStr, index));
                        Files.move(path, Paths.get(OUTPUT_PATH + "/" + path.getFileName()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        ).get();
    }

    private static Consumer<? super String> cmd() {
        return string -> {
//            try {
//                String fullCmd = "echo '%s' | curl -d @- http://localhost:9200/amazon/review/%d";
//                Runtime.getRuntime().exec(String.format(fullCmd, string, INDEX.getAndIncrement()));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        };
    }

    private static Consumer<? super String> pushObj2Db(String fileName, AtomicInteger INDEX) {

        return string -> {
            String idTxt = fileName.substring(8, fileName.length() - 7);
            int index = INDEX.getAndIncrement();
            String url = "http://localhost:9200/amazon/review/" + idTxt + index;
//            System.out.println(url);
            URL obj = null;
            try {
                obj = new URL(url);
//                long timestamp1 = System.currentTimeMillis();
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//                long timestamp2 = System.currentTimeMillis();
                //add reuqest header
                con.setRequestMethod("PUT");
                con.setRequestProperty("Content-Type", "application/json");

                // Send post request
                con.setDoOutput(true);
                try(OutputStreamWriter osw = new OutputStreamWriter(con.getOutputStream())) {
                    osw.write(string);
                    osw.flush();


//                if (index % 2 == 0) {
                    getResponse(con, index);
                }
//                } else {
//                    schedule(con, index);
//                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
    }

    private static void getResponse(HttpURLConnection con, int index) {
        try {
            int rsCode = con.getResponseCode();
            if (rsCode < 200 || rsCode >= 300) {
                System.out.println(rsCode + ' ' + con.getResponseMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        con.disconnect();
        if (index % 100 == 0) {
            System.out.println(String.format("Processed %d entries!", index));
        }
    }
//
//    private static void schedule(HttpURLConnection con, int index) {
//        closingPool.submit(() -> {
//            getResponse(con, index);
//        });
//
//    }


    public static List<Path> getFiles() throws IOException {
        return Files.list(Paths.get(PATH)).collect(Collectors.toList());
    }
}
