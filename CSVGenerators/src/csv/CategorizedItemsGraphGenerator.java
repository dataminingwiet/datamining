package csv;

import pojo.Connection;
import pojo.Item;

import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CategorizedItemsGraphGenerator {

    public String getNodesHeader() {
        return "ID,Label,Category1";
    }

    public String getEdgeHeader() {
        return "Source,Target,Type";
    }

    private class Node {
        String id;
        String category;
        String label;
    }

    public Consumer<? super Item> consumeItem(PrintWriter nodesWriter, PrintWriter edgeWriter) {
        return (item) -> {

            List<Node> nodes = getNodes(item);
            Optional.ofNullable(getNodeStr(nodes)).filter(not(String::isEmpty)).ifPresent(nodesWriter::println);
            Optional.ofNullable(getEdgeStr(item, nodes)).filter(not(String::isEmpty)).ifPresent(edgeWriter::println);

            nodesWriter.flush();
            edgeWriter.flush();
            System.gc();
        };
    }

    private String getEdgeStr(Item item, List<Node> nodes) {
        if (item != null && item.related != null && nodes != null) {
            return item.related.stream()
                    .filter(Objects::nonNull)
                    .map(formatConnections(nodes))
                    .map(String::trim)
                    .filter(not(String::isEmpty))
                    .collect(Collectors.joining("\n"));
        }
        return null;
    }

    private Function<Connection, String> formatConnections(List<Node> nodes) {
        return (conn) -> {
            if (nodes != null) {
                return nodes.stream().map(id -> {
                    StringJoiner result = new StringJoiner("\n");
                    result = conn.alsoViewed ? result.add(String.format("%s,%s,%s",id, conn.target, "ALSO_VIEWED")) : result;
                    result = conn.alsoBought ? result.add(String.format("%s,%s,%s",id, conn.target, "ALSO_BOUGHT")) : result;
                    result = conn.boughtTogether ? result.add(String.format("%s,%s,%s",id, conn.target, "BOUGHT_TOGETHER")) : result;
                    result = conn.buyAfterViewing ? result.add(String.format("%s,%s,%s",id, conn.target, "BUY_AFTER_VIEWING")) : result;

                    return result.toString();
                })
                .collect(Collectors.joining("\n"));

            }
            return  "";
        };
    }

    private String getNodeStr(List<Node> nodes) {
        if (nodes != null) {
            return nodes.stream()
                    .map(node -> String.format("%s,%s,%s", node.id, safe(node.label), safe(node.category)))
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining("\n"));
        }
        return null;
    }

    private List<Node> getNodes(Item item) {
        return item.categories.stream()
                .filter(Objects::nonNull)
                .filter(not(List::isEmpty))
                .map(list -> list.get(0))
                .filter(Objects::nonNull)
                .map(createNode(item.id, item.title))
                .collect(Collectors.toList());
    }

    private Function<String, Node> createNode(String id, String title) {
        return cat ->  {
            Node node = new Node();
            node.id = id + (cat != null ? cat.hashCode() : "XXX");
            node.label = title;
            node.category =cat;
            return node;
        };
    }

    private <T> Predicate<T> not(Predicate<T> predicate) {
        return predicate.negate();
    }

    private String safe(String str) {
        str = str == null ? "" : str;
        return '\"' + str.replaceAll("\"", "\"\"") + '\"';
    }
}
