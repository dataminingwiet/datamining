package csv;

import pojo.Connection;
import pojo.Item;

import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CategoryAggregationGraphGenerator {
    private static final int GC_INTERVAL = 10;
    private static final int LOG_INTERVAL = 10000;
    private static boolean logging = false;
    private AtomicInteger logCountDownTimer= new AtomicInteger(GC_INTERVAL);
    private AtomicInteger countDownTimer= new AtomicInteger(GC_INTERVAL);
    private Set<String> categoryNames = new HashSet<>();
    private Map<String, Set<String>> nodeCategoriesMap = new HashMap<>();
    private Map<Edge, Integer> edgesPowerA = new HashMap<>();
    private Map<Edge, Integer> edgesPowerB = new HashMap<>();
    private Map<Edge, Integer> edgesPowerC = new HashMap<>();
    private Map<Edge, Integer> edgesPowerD = new HashMap<>();

    public String getNodesHeader() {
        return "ID,Label";
    }

    public String getEdgeHeader() {
        return "Source,Target,Type,Strength";
    }

    public void onProcessed(PrintWriter nodesWriter, PrintWriter edgeWriter) {
        String alsoViewed = "ALSO_VIEWED";
        String alsoBought = "ALSO_BOUGHT";
        String boughtTogether = "BOUGHT_TOGETHER";
        String buyAfterViewing = "BUY_AFTER_VIEWING";

        dumpToFile(edgeWriter, alsoViewed, edgesPowerA);
        dumpToFile(edgeWriter, alsoBought, edgesPowerB);
        dumpToFile(edgeWriter, boughtTogether, edgesPowerC);
        dumpToFile(edgeWriter, buyAfterViewing, edgesPowerD);
    }

    private void dumpToFile(PrintWriter edgeWriter, String alsoViewed, Map<Edge, Integer> edgesPower) {
        edgesPower.entrySet().stream()
                .map(edgeInfoToString(alsoViewed))
                .filter(Objects::nonNull)
                .peek(x -> memoryClean(edgeWriter))
                .forEach(edgeWriter::println);
    }

    private Function<Map.Entry<Edge, Integer>, String> edgeInfoToString(String alsoViewed) {
        return entry -> {
            if (entry.getValue() != null && entry.getValue() > 0) {
                return String.format("%s,%s,%s,%d", entry.getKey().source, entry.getKey().target, alsoViewed, entry.getValue());
            }
            return null;
        };
    }

    private void memoryClean(PrintWriter pw) {
        if (countDownTimer.getAndDecrement() <= 0) {
            countDownTimer.set(GC_INTERVAL);
            pw.flush();
            System.gc();
        }
        if (logging && logCountDownTimer.getAndDecrement() <= 0) {
            countDownTimer.set(LOG_INTERVAL);
            System.out.println(
                    String
                            .format("EDGES: %s, %s, %s, %s",
                                    edgesPowerA.size(),
                                    edgesPowerB.size(),
                                    edgesPowerC.size(),
                                    edgesPowerD.size()));
        }
    }

//    private class Node {
//        String id;
//        String category;
//        String label;
//    }

    private class Edge {
        Integer source;
        Integer target;

        public Edge(Integer source, Integer target) {
            this.source = source;
            this.target = target;
        }

        @Override
        public int hashCode() {
            return (source == null ? 1: source.hashCode())
                    * (target == null ? 1 : target.hashCode());
        }

        @Override
        public boolean equals(Object o) {
            if ( o instanceof Edge) {
                Edge e = (Edge) o;
                return  ((this.source.equals(e.source)  && this.target.equals(e.target))
                                || (this.target.equals(e.source)  && this.source.equals(e.target)) );
            }
            return  false;
        }
    }

    public Consumer<? super Item> generateNodes(PrintWriter nodesWriter) {
        return (item) -> {

            if (item.title == null || item.title.isEmpty()) {
                return;
            }

            if (item.categories != null) {

                Set<String> set = item.categories.stream()
                        .filter(not(List::isEmpty))
                        .map(l -> l.get(0))
                        .filter(Objects::nonNull)
                        .filter(not(String::isEmpty))
                        .collect(Collectors.toSet());

                if (!set.isEmpty()) {
                    nodeCategoriesMap.put(item.id, set);
                    Optional.ofNullable(getNodeString(set))
                            .map(String::trim)
                            .filter(not(String::isEmpty))
                            .ifPresent(nodesWriter::println);
                }
                memoryClean(nodesWriter);
            }
        };
    }

    public Consumer<? super Item> generateEdges(PrintWriter edgesWriter) {
        logging = true;
        return (item) -> {

            if (item.title == null || item.title.isEmpty() || nodeCategoriesMap.get(item.id) == null) {
                return;
            }

            processEdges(item, edgesWriter);
        };
    }

    private String getNodeString(Set<String> set) {
            return set.stream()
                    .filter(not(categoryNames::contains))
                    .peek(categoryNames::add)
                    .map(cat -> String.format("%s,%s", cat.hashCode(), safe(cat)))
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining("\n"));
    }

    private void processEdges(Item item, PrintWriter edgesWriter) {
        if (item != null && item.related != null) {
            item.related.stream()
                    .filter(Objects::nonNull)
                    .forEach(updateConnections(item, edgesWriter));
        }
    }

    private Consumer<Connection> updateConnections(Item item, PrintWriter edgesWriter) {
        return (conn) -> {
            nodeCategoriesMap.get(item.id).stream()
                    .forEach(cat -> {
                        if(item.related != null) {
//                            String a = "," + cat.hashCode() + alsoViewed;
//                            String b = "," + cat.hashCode() + alsoBought;
//                            String c = "," + cat.hashCode() + boughtTogether;
//                            String d = "," + cat.hashCode() + buyAfterViewing;

                            item.related.stream()
                                    .filter(Objects::nonNull)
                                    .forEach(con -> {
                                        if (con.alsoViewed) {
                                            updateEdges(edgesPowerA, cat, nodeCategoriesMap.get(con.target));
                                        }

                                        if (con.alsoBought) {
                                            updateEdges(edgesPowerB, cat, nodeCategoriesMap.get(con.target));
                                        }

                                        if (con.boughtTogether) {
                                            updateEdges(edgesPowerC, cat, nodeCategoriesMap.get(con.target));
                                        }

                                        if (con.buyAfterViewing) {
                                            updateEdges(edgesPowerD, cat, nodeCategoriesMap.get(con.target));
                                        }
                                    });
                        }
                });
                memoryClean(edgesWriter);
            };
    }

    private void updateEdges(Map<Edge, Integer> edgesPower, String cat, Set<String> strings) {
        if (strings != null) {
            strings.forEach(relatedCat -> {
                Edge edge = new Edge(cat.hashCode(), relatedCat.hashCode());
                Integer power = edgesPower.get(edge);
                power = power == null ? 0: power;
                edgesPower.put(edge, power + 1);
            });
        }
    }

//    private String getNodeStr(List<Node> nodes) {
//        if (nodes != null) {
//            return nodes.stream()
//                    .map(node -> String.format("%s,%s,%s", node.id, safe(node.label), safe(node.category)))
//                    .filter(Objects::nonNull)
//                    .collect(Collectors.joining("\n"));
//        }
//        return null;
//    }

//    private List<Node> getNodes(Item item) {
//        return item.categories.stream()
//                .filter(Objects::nonNull)
//                .filter(not(List::isEmpty))
//                .map(list -> list.get(0))
//                .filter(Objects::nonNull)
//                .map(createNode(item.id, item.title))
//                .collect(Collectors.toList());
//    }

//    private Function<String, Node> createNode(String id, String title) {
//        return cat ->  {
//            Node node = new Node();
//            node.id = id;// + (cat != null ? cat.hashCode() : "XXX");
//            node.label = title;
//            node.category =cat;
//            return node;
//        };
//    }

    private <T> Predicate<T> not(Predicate<T> predicate) {
        return predicate.negate();
    }

    private String safe(String str) {
        str = str == null ? "" : str;
        return '\"' + str.replaceAll("\"", "\"\"") + '\"';
    }
}
