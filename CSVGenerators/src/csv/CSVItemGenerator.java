package csv;

import pojo.Connection;
import pojo.Item;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CSVItemGenerator {

    private static final int THROTTLE_ITEMS_COUNT = 20;
    private AtomicInteger flushCountDown = new AtomicInteger(THROTTLE_ITEMS_COUNT);

    public String getNodesHeader() {
        return "ID,Label,Price,Category1,Category2,Category3";
    }

    public String getEdgeHeader() {
        return "Source,Target,Type";
    }

    public Consumer<? super Item> consumeItem(PrintWriter nodesWriter, PrintWriter edgeWriter) {
        return (item) -> {
            String id = item.id;
            String title = item.title;
            if (title == null || title.isEmpty() || item.id == null || item.id.isEmpty()) {
                return;
            }
            BigDecimal price = Optional.ofNullable(item.price).map(p -> p.setScale(2, BigDecimal.ROUND_DOWN)).orElse(BigDecimal.ZERO);
            String category1 = getCategory(item, 0);
            String category2 = getCategory(item, 1);
            String category3 = getCategory(item, 2);

            String record = new StringJoiner(",")
                    .add(id)
                    .add(safe(title))
                    .add(price.toString())
                    .add(safe(category1))
                    .add(safe(category2))
                    .add(safe(category3))
                    .toString();

            nodesWriter.println(record);

            getEdgeStr(item).forEach(edgeWriter::println);
            handleFlush(nodesWriter, edgeWriter);
        };
    }

    private void handleFlush(PrintWriter nodesWriter, PrintWriter edgeWriter) {
        if (flushCountDown.getAndDecrement() < 0) {
            flushCountDown.set(THROTTLE_ITEMS_COUNT);
            nodesWriter.flush();
            edgeWriter.flush();
            System.gc();
        }
    }

    private String getCategory(Item item, int catIndex) {
        return Optional.ofNullable(item.categories)
                .filter(list -> list.size() > catIndex).map(list->list.get(catIndex))
                .filter(not(List::isEmpty)).map(list->list.get(0))
                .orElse("");
    }

    private List<String> getEdgeStr(Item item) {
        if (item != null && item.related != null) {
            return item.related.stream()
                    .filter(Objects::nonNull)
                    .map(formatConnections(item.id))
                    .flatMap(List::stream)
                    .map(String::trim)
                    .filter(not(String::isEmpty))
                    .collect(Collectors.toList());
        }
        return null;
    }

    private Function<Connection, List<String>> formatConnections(String id) {
        return (conn) -> {
                    List<String> result = new LinkedList<>();
                    if (conn.alsoViewed)
                        result.add(String.format("%s,%s,%s", id, conn.target, "ALSO_VIEWED"));
                    if (conn.alsoBought)
                        result.add(String.format("%s,%s,%s", id, conn.target, "ALSO_BOUGHT"));
                    if (conn.boughtTogether)
                        result.add(String.format("%s,%s,%s", id, conn.target, "BOUGHT_TOGETHER"));
                    if (conn.buyAfterViewing)
                        result.add(String.format("%s,%s,%s", id, conn.target, "BUY_AFTER_VIEWING"));

                    return result;
        };
    }

    private <T> Predicate<T> not(Predicate<T> predicate) {
        return predicate.negate();
    }

    private String safe(String str) {
        str = str == null ? "" : str;
        return '\"' + str.replaceAll("\"", "\"\"") + '\"';
    }
}
