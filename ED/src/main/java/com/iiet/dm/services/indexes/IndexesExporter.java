package com.iiet.dm.services.indexes;

import com.iiet.dm.services.utils.PrintWriterExporter;

import java.io.*;
import java.util.Objects;
import java.util.Set;

/**
 * Saves indexes to file
 */
public class IndexesExporter extends PrintWriterExporter {

    private PrintWriter indexesPW;

    /**
     * Default constructor
     */
    public IndexesExporter() throws IOException {
        this(Constants.DEAFAULT_OUTPUT_FILNAME);
    }

    /**
     * Parameterized constructor
     *
     * @param fileName name of file for the indexes to be saved in
     */
    public IndexesExporter(String fileName) throws IOException {
        indexesPW = new PrintWriter(new FileOutputStream(new File(fileName)));
    }

    /**
     * Exports indexes set
     *
     * @param indexSet set to be exported
     * @return this object
     */
    public IndexesExporter export(Set<String> indexSet) {
        if (Objects.nonNull(indexSet)) {
            indexSet.forEach(indexesPW::println);
        }
        return this;
    }

    @Override
    protected PrintWriter getPrintWriter() {
        return this.indexesPW;
    }
}
