package com.iiet.dm.services.analysis.export;

import com.iiet.dm.services.analysis.pojo.AnalysisResult;
import com.iiet.dm.services.csv.RawCsvExporter;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.StringJoiner;

public class SentimentAnalysisDataExporter {

    private static final String HEADER = "unixTimeReview,sentiment1,sentiment2,overall,asin,reviewerId";
    private RawCsvExporter rawCsvExporter;

    public SentimentAnalysisDataExporter() throws FileNotFoundException {
        rawCsvExporter = new RawCsvExporter("sentimentAnalysisData.csv", HEADER);
    }

    public void export(List<AnalysisResult> results) {
        results.stream()
                .filter(res -> res.sentiment2 > -2)
                .map(result ->
            new StringJoiner(",")
                    .add(String.valueOf(result.unixReviewTime))
                    .add(String.valueOf(result.sentiment1))
                    .add(String.valueOf(result.sentiment2))
                    .add(String.valueOf(result.overall))
                    .add(result.asin)
                    .add(result.reviewerId)
                    .toString()
        ).forEach(rawCsvExporter::export);
        rawCsvExporter.flushAndClose();
    }
}
