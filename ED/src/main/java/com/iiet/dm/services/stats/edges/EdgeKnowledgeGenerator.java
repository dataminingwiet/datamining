package com.iiet.dm.services.stats.edges;

import com.iiet.dm.services.metadata.pojo.Connection;
import com.iiet.dm.services.metadata.pojo.Item;
import com.iiet.dm.services.stats.nodes.pojo.ReviewerStats;
import org.elasticsearch.search.SearchHit;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class EdgeKnowledgeGenerator {
    private static final char QUOTE = '"';
    private static final String
            HEADER = "Item,AlsoViewLen,AlsoBoughtLengthLen,BoughtTogetherLen,BuyAfterViewingLen";

//    private Map<String, MetadataStats> stats = new HashMap<>();
    private static Set<String> handledIndexes = new HashSet<>();

    public String getResultCsvHeader() {
        return HEADER;
    }

    /**
     * Translates Metadata Item to String CSV Record
     *
     * @param item metadata item to be transformed
     * @return csv translation result
     */
    public String consume(Item item) {
        if (!handledIndexes.contains(item.id)) {
            handledIndexes.add(item.id);
            long alsoViewed = getLength(item, con -> con.alsoViewed);
            long alsoBought = getLength(item, con -> con.alsoBought);
            long boughtTogether = getLength(item, con -> con.boughtTogether);
            long buyAfterViewing = getLength(item, con -> con.buyAfterViewing);
            if (alsoBought == 0
                    && alsoBought == 0
                    && boughtTogether == 0
                    && buyAfterViewing == 0) {
                return "";
            }
            return new StringJoiner(",")
                    .add(item.id)
                    .add(String.valueOf(alsoViewed))
                    .add(String.valueOf(alsoBought))
                    .add(String.valueOf(boughtTogether))
                    .add(String.valueOf(buyAfterViewing))
                    .toString();
        } else {
            System.out.println("Duplicate found!");
        }
        return "";
    }

    private long getLength(Item item, Predicate<Connection> fn) {
        return item.related.stream().filter(fn).count();
    }
}
