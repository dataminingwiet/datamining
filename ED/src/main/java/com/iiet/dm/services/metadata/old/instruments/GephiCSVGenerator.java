package com.iiet.dm.services.metadata.old.instruments;

import com.iiet.dm.services.indexes.IndexesImporter;
import com.iiet.dm.services.metadata.transformer.MetadataPojo2CsvEdges;
import com.iiet.dm.services.metadata.parser.Parser;
import com.iiet.dm.services.metadata.pojo.Item;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import static java.util.Objects.nonNull;

/**
 * Gephi Csv Edge Generator, upon metadata.
 */
public class GephiCSVGenerator {

    private PrintWriter edgeWriter;
    private Parser parser = new Parser();
    private MetadataPojo2CsvEdges simpleGenerator ;

    public GephiCSVGenerator() {
        simpleGenerator = new MetadataPojo2CsvEdges(new IndexesImporter().importIndexes());
    }

    public void run(String inputPath, String outputfolder) throws IOException {
        new File(outputfolder).mkdirs();

        File edgesFile = new File(outputfolder + "/books_edges.csv");

        edgeWriter = new PrintWriter(edgesFile);

        edgeWriter.println(simpleGenerator.getEdgeHeader());

        Files.lines(Paths.get(inputPath))
                .map(parser::parseItem)
                .filter(Objects::nonNull)
                .filter(this::isItemComplete)
                .forEach(simpleGenerator.exportToCsv(edgeWriter));

        edgeWriter.flush();
        edgeWriter.close();
    }

    private boolean isItemComplete(Item item) {
        return nonNull(item.id) && notEmpty(item.id)
                && nonNull(item.title) && notEmpty(item.title)
                && nonNull(item.price) && item.price.doubleValue() > 0;
    }

    private boolean notEmpty(String str) {
        return !str.isEmpty();
    }
}
