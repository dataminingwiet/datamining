package com.iiet.dm.services.json;

import com.iiet.dm.services.utils.PrintWriterExporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Raw Json exporter.
 */
public class RawJsonExporter extends PrintWriterExporter {

    private final PrintWriter resultJsonPW;
    private final AtomicInteger counter = new AtomicInteger(0);
    private final int FLUSH_RATE = 1000;
    private static String RESULT_JSON_FILE_NAME = "indexed.json";


    /**
     * Default constructor.
     */
    public RawJsonExporter() throws FileNotFoundException {
        this(RESULT_JSON_FILE_NAME);
    }

    /**
     * Parametrized constructor
     *
     * @param fileName filename of output
     */
    public RawJsonExporter(String fileName) throws FileNotFoundException {
        resultJsonPW = new PrintWriter(new FileOutputStream(new File(fileName)));
    }

    /**
     * Exports json to file
     *
     * @param line json to be exported
     */
    public void export(String line) {
        if (!line.isEmpty()) {
            resultJsonPW.println(line);
            if (counter.getAndIncrement() % FLUSH_RATE == 0) {
                resultJsonPW.flush();
            }
        }
    }

    @Override
    protected PrintWriter getPrintWriter() {
        return this.resultJsonPW;
    }
}
