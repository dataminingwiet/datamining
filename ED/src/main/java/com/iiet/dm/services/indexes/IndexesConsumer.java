package com.iiet.dm.services.indexes;

import org.elasticsearch.search.SearchHit;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Consumes indexes
 */
public class IndexesConsumer {

    private Set<String> indexes = new HashSet<>();

    /**
     * Consumes id upon whole searchhit.
     *
     * @param hit search hit to be consumed
     */
    public void accept(SearchHit hit) {
        Optional.ofNullable(hit).map(SearchHit::getSourceAsMap)
                .map(map -> map.get("asin"))
                .filter(Objects::nonNull)
                .map(String.class::cast)
                .ifPresent(this::accept);
    }

    private <T> Predicate<T> not(Predicate<T> predicate) {
        return predicate.negate();
    }

    /**
     * Consumes id.
     *
     * @param id id to be consumed
     */
    public void accept(String id) {
        Optional.ofNullable(id)
                .map(String::trim)
                .filter(not(String::isEmpty))
                .ifPresent(indexes::add);
    }

    /**
     * Indexes accessor.
     *
     * @return set of indexes
     */
    public Set<String> getIndexes() {
        return this.indexes;
    }


}
