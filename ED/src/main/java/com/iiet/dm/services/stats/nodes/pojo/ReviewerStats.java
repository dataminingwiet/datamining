package com.iiet.dm.services.stats.nodes.pojo;

public class ReviewerStats {

    private double helpfulnessTotal;
    private double lengthTotal;
    private double reviewCount;
    private String reviewerName;
    private int sentimentTotal;

    public double getHelpfulnessTotal() {
        return helpfulnessTotal;
    }

    public void setHelpfulnessTotal(double helpfullnessTotal) {
        this.helpfulnessTotal = helpfullnessTotal;
    }

    public double getLengthTotal() {
        return lengthTotal;
    }

    public void setLengthTotal(double lengthTotal) {
        this.lengthTotal = lengthTotal;
    }

    public double getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(double reviewCount) {
        this.reviewCount = reviewCount;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReviewerName() {
        return this.reviewerName;
    }

    public int getSentimentTotal() {
        return sentimentTotal;
    }

    public void setSentimentTotal(int val) {
        this.sentimentTotal = val;
    }
}
