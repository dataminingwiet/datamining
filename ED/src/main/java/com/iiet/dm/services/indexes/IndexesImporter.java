package com.iiet.dm.services.indexes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Saves indexes to file
 */
public class IndexesImporter {

    private String filename;

    /**
     * Default constructor
     */
    public IndexesImporter() {
        this(Constants.DEAFAULT_OUTPUT_FILNAME);
    }

    /**
     * Parameterized constructor
     *
     * @param fileName name of file for the indexes to be saved in
     */
    public IndexesImporter(String fileName) {
        this.filename = fileName;
    }

    /**
     * Imports indexes set
     *
     * @return this object
     */
    public Set<String> importIndexes() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(this.filename)))){
            return bufferedReader.lines()
                    .map(String::trim)
                    .filter(not(String::isEmpty))
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptySet();
        }
    }

    private <T> Predicate<T> not(Predicate<T> predicate) {
        return predicate.negate();
    }
}
