package com.iiet.dm.services.merge;

import com.iiet.dm.services.metadata.parser.Parser;
import com.iiet.dm.services.metadata.pojo.Item;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class DecorateNodesWithMetadata {
    private static Map<String, String> reviews = new HashMap<>();
    private static PrintWriter resultPrintWriter;

    public static void main(String[] args) throws IOException {
        buildSet();
        setUpOutput();
        String filename = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";
        String filename2 = "important_books_metadata.json";
        resultPrintWriter.println("ID,unixReviewTime,sentiment,overall,summary,reviewerID,reviewerName,helpful,reviewText,reviewTime,productTitle,productPrice,productRank");
        Files.newBufferedReader(Paths.get(filename)).lines()
                .map(new Parser()::parseItem)
                .filter(Objects::nonNull)
                .forEach(handleLine());
        System.out.println("Reviews for products without metadata: " + reviews.size());
        finalizeResultWriter();
    }

    private static Consumer<? super Item> handleLine() {

        return item -> {
            if (reviews.containsKey(item.id)) {
                String result = reviews.remove(item.id) + ',' + getMetadataInfo(item);
                resultPrintWriter.println(result);
            }
        };
    }

    private static String getMetadataInfo(Item item) {
        BigDecimal price = Optional.ofNullable(item.price).map(p -> p.setScale(2, BigDecimal.ROUND_DOWN)).orElse(BigDecimal.ZERO);
        String category1 = getCategory(item, 0);
        String subcategory1 = getSubCategory(item, 0);
        String category2 = getCategory(item, 1);
        String subcategory2 = getSubCategory(item, 1);
        String category3 = getCategory(item, 2);
        String subcategory3 = getSubCategory(item, 2);

        return new StringJoiner(",")
                .add(safe(item.title))
                .add(price.toString())
//                .add(safe(category1))
//                .add(safe(subcategory1))
//                .add(safe(category2))
//                .add(safe(subcategory2))
//                .add(safe(category3))
//                .add(safe(subcategory3))
                .add(Optional.ofNullable(item.ranks.get("Books")).map(Object::toString).orElse(""))
                .toString();
    }

    private static String getCategory(Item item, int catIndex) {
        return Optional.ofNullable(item.categories)
                .filter(list -> list.size() > catIndex).map(list -> list.get(catIndex))
                .filter(not(List::isEmpty)).map(list -> list.get(0))
                .orElse("");
    }

    private static String getSubCategory(Item item, int catIndex) {
        return Optional.ofNullable(item.categories)
                .filter(list -> list.size() > catIndex).map(list -> list.get(catIndex))
                .filter(list -> list.size() > 2).map(list -> list.get(1))
                .orElse("");
    }

    private static <T> Predicate<T> not(Predicate<T> p) {
        return p.negate();
    }


    private static String safe(String str) {
        str = str == null ? "" : str;
        return '\"' + str.replaceAll("\"", "\"\"") + '\"';
    }

    private static void finalizeResultWriter() {
        resultPrintWriter.flush();
        resultPrintWriter.close();
    }

    private static void setUpOutput() throws FileNotFoundException {
        resultPrintWriter = new PrintWriter(new FileOutputStream(new File("merged.com.iiet.dm.services.metadata.old.csv")));
    }

    private static void buildSet() throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get("/home/robert/AGHStudia/ED/Gephi/books_sentiment/indexed.com.iiet.dm.services.metadata.old.csv"));
        br.lines().skip(1).forEach(str -> {
            if (str.length() > 11) {
                reviews.put(str.substring(1, 11), str);
            } else {
                System.out.println(str);
            }

        });
    }
}
