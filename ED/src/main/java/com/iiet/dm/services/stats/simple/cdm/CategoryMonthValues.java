package com.iiet.dm.services.stats.simple.cdm;

public class CategoryMonthValues {
    public String key;
    public String category;
    public int month;
    public int year;
    public double sentimentTotal;
    public int reviewCount;
}
