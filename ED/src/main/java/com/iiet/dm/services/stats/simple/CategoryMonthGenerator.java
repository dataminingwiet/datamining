package com.iiet.dm.services.stats.simple;

import com.iiet.dm.services.metadata.pojo.Connection;
import com.iiet.dm.services.metadata.pojo.Item;
import com.iiet.dm.services.stats.simple.cdm.CategoryMonthValues;
import org.elasticsearch.search.SearchHit;
import org.joda.time.DateTime;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class CategoryMonthGenerator {
    private static final char QUOTE = '"';
    private static final String
            HEADER = "CategoryMonthYear,Category,Month,Year,timeOrder,avgSentiment,count";

    private static Map<String, Set<String>> categoriesMap = new HashMap<>();
    private static Map<String, CategoryMonthValues> resultMap = new HashMap<>();

    public String getResultCsvHeader() {
        return HEADER;
    }

    /**
     * Translates Metadata Item to String CSV Record
     *
     * @param hit review item to be transformed
     * @return csv translation result
     */
    public void buildResultObject(SearchHit hit) {
        Map<String, Object> sourceAsMap = hit.getSourceAsMap();
        int sentiment = (int) sourceAsMap.getOrDefault("sentiment", Integer.MIN_VALUE);
        long time = Long.parseLong(sourceAsMap.getOrDefault("unixReviewTime", Integer.MIN_VALUE).toString());
        String productId = (String) sourceAsMap.getOrDefault("asin", "");

        DateTime dateTime = new DateTime(time * 1000);

        Set<String> categories = categoriesMap.get(productId);
        if (categories != null) {
            categories.forEach(cat -> {
                String key = getKeyForResultMap(cat, dateTime);
                CategoryMonthValues categoryMonth = resultMap.get(cat);
                if (categoryMonth == null) {
                    categoryMonth = new CategoryMonthValues();
                    categoryMonth.key = key;
                    categoryMonth.category = cat;
                    categoryMonth.month = dateTime.getMonthOfYear();
                    categoryMonth.reviewCount = 0;
                    categoryMonth.sentimentTotal = 0;
                    categoryMonth.year = dateTime.getYear();
                }
                categoryMonth.reviewCount += 1;
                categoryMonth.sentimentTotal += sentiment;
                resultMap.put(key, categoryMonth);
            });
        }
    }

    /**
     * Gets results as stream
     *
     * @return stream of results
     */
    public Stream<String> getResultsAsStream() {
        categoriesMap.clear();
        return resultMap.values().stream()
                .filter(val -> val.reviewCount > 0)
                .filter(val -> val.category != null && !val.category.trim().isEmpty())
                .map(val -> new StringJoiner(",")
                        .add(QUOTE + val.key + QUOTE)
                        .add(QUOTE + val.category + QUOTE)
                        .add(String.valueOf(val.month))
                        .add(String.valueOf(val.year))
                        .add(String.valueOf(val.year * 100 + val.month))
                        .add(String.valueOf(val.sentimentTotal / val.reviewCount))
                        .add(String.valueOf(val.reviewCount))
                        .toString());
    }

    private String getKeyForResultMap(String cat, DateTime date) {
        return cat + "//" + date.getMonthOfYear() + "/" + date.getYear();
    }

    private long getLength(Item item, Predicate<Connection> fn) {
        return item.related.stream().filter(fn).count();
    }

    public void buildMapOfCategories(Item item) {
        Set<String> categoriesSet = getCategories(item);
        if (!categoriesSet.isEmpty()) {
            categoriesMap.put(item.id, categoriesSet);
        }

    }

    /**
     * Note: Depth of catgories = 2.
     */
    private Set<String> getCategories(Item item) {
        return Optional.ofNullable(item)
                .map(i -> i.categories)
                .filter(list -> !list.isEmpty())
                .map(list -> list.stream())
                .orElseGet(Stream::empty)
                .filter(arr -> arr.size() > 0)
                .map(arr -> arr.get(0))
                .collect(Collectors.toSet());
    }
}
