package com.iiet.dm.services.metadata.pojo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Item {
    public String id;
    public List<List<String>> categories;
    public String title;
    public BigDecimal price;
    public Map<String, BigInteger> ranks;
    public Set<Connection> related;

}

