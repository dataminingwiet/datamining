package com.iiet.dm.services.analysis.second;

import com.iiet.dm.services.analysis.pojo.AnalysisResult;
import com.iiet.dm.services.downloader.QuerableElasticSearchDownloader;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class SecondClassifierDataDecorator {

    private QuerableElasticSearchDownloader elasticSearchDownloader;
    private AtomicInteger decorated = new AtomicInteger(0);

    public SecondClassifierDataDecorator() {
        this.elasticSearchDownloader = new QuerableElasticSearchDownloader();
    }

    public void decorate(List<AnalysisResult> results) {
        try {
            this.elasticSearchDownloader.init();
            results.forEach(this::decorate);
            this.elasticSearchDownloader.tearDown();
            System.out.println("Analysis records decorated!");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private void decorate(AnalysisResult analysisResult) {
        QueryBuilder withSentiment = QueryBuilders.existsQuery("sentiment");
        QueryBuilder withReviewerId = QueryBuilders.termQuery("reviewerID", analysisResult.reviewerId);
        QueryBuilder withAsin = QueryBuilders.termQuery("asin", analysisResult.asin);

        QueryBuilder query = QueryBuilders.boolQuery().must(withSentiment).must(withReviewerId);

        elasticSearchDownloader.download(withReviewerId, updateSecondSentiment(analysisResult));
        if (decorated.incrementAndGet() % 100 == 0) {
            System.out.println("Analysis records decorated: " + decorated);
        }
    }

    private Consumer<SearchHit> updateSecondSentiment(AnalysisResult analysisResult) {

        return (searchHit) -> {
            try {
                Map<String, Object> sam = searchHit.getSourceAsMap();
                Object sentiment = sam.get("sentiment");
                Object revId = sam.get("reviewerID");
                if (analysisResult.reviewerId.equals(revId) && sentiment != null) {
                    analysisResult.sentiment2 = Double.parseDouble(sentiment.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        };
    }
}
