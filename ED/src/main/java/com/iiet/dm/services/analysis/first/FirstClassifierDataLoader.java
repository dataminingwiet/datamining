package com.iiet.dm.services.analysis.first;

import com.iiet.dm.services.analysis.pojo.AnalysisResult;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

public class FirstClassifierDataLoader {
    private static final String FILENAME = "2013.csv";
    public List<AnalysisResult> readResultsOfFirstClassifier() throws IOException {
        CSVParser parser = new CSVParser(new InputStreamReader(new FileInputStream(new File(FILENAME))), CSVFormat.DEFAULT);


        return parser.getRecords().stream().skip(1).map(record -> {
            // reviewerID,asin,sentiment,unixReviewTime,overall
            AnalysisResult ar = new AnalysisResult();
            ar.reviewerId = record.get(0);
            ar.asin = record.get(1);
            ar.sentiment1 = Double.parseDouble(record.get(2));
            ar.sentiment2 = Double.NEGATIVE_INFINITY;
            ar.unixReviewTime = Long.parseLong(record.get(3));
            ar.overall = Double.parseDouble(record.get(4));
            return ar;
        }).collect(Collectors.toList());
    }
}
