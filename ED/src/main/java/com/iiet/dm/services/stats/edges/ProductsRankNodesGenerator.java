package com.iiet.dm.services.stats.edges;

import com.iiet.dm.services.metadata.pojo.Connection;
import com.iiet.dm.services.metadata.pojo.Item;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class ProductsRankNodesGenerator {
    private static final char QUOTE = '"';
    private static final String
            HEADER = "ID,Title,Price";
    
    private static Set<String> handledIndexes = new HashSet<>();

    public String getResultCsvHeader() {
        return HEADER;
    }

    /**
     * Translates Metadata Item to String CSV Record
     *
     * @param item metadata item to be transformed
     * @return csv translation result
     */
    public String consume(Item item) {
        if (!handledIndexes.contains(item.id)) {
            handledIndexes.add(item.id);
            return new StringJoiner(",")
                    .add(item.id)
                    .add(asStr(item.title))
                    .add(String.valueOf(item.price))
                    .toString();
        } else {
            System.out.println("Duplicate found!");
        }
        return "";
    }

    private CharSequence asStr(String reviewText) {
        return QUOTE + escapeString(reviewText) + QUOTE;
    }

    private String escapeString(String string) {
        return string.trim()
                .replaceAll("\"", "'")
                .replaceAll("\n", "\\n");
    }
}
