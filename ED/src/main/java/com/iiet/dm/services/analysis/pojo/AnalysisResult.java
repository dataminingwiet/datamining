package com.iiet.dm.services.analysis.pojo;

public class AnalysisResult {
    public String reviewerId;
    public String asin;
    public double sentiment1;
    public double sentiment2;
    public long unixReviewTime;
    public double overall;
}
