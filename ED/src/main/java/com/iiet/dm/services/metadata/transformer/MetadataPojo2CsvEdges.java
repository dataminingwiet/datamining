package com.iiet.dm.services.metadata.transformer;

import com.iiet.dm.services.metadata.pojo.Connection;
import com.iiet.dm.services.metadata.pojo.Item;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MetadataPojo2CsvEdges {

    private static final int THROTTLE_ITEMS_COUNT = 10000;
    private final Set<String> indexes;
    private AtomicInteger flushCountDown = new AtomicInteger(0);

    public MetadataPojo2CsvEdges(Set<String> indexes) {
        this.indexes = indexes;
    }

    public String getEdgeHeader() {
        return "Source,Target,Type";
    }

    public Consumer<? super Item> exportToCsv(PrintWriter edgeWriter) {
        return (item) -> {
            getEdgeStr(item).forEach(edgeWriter::println);
            handleFlush(edgeWriter);
        };
    }

    private void handleFlush(PrintWriter edgeWriter) {
        if (flushCountDown.getAndIncrement() % THROTTLE_ITEMS_COUNT == 0) {
            edgeWriter.flush();
            System.gc();
            System.out.println(flushCountDown.get());
        }
    }

    private List<String> getEdgeStr(Item item) {
        if (item != null && item.related != null) {
            return item.related.stream()
                    .filter(Objects::nonNull)
                    .map(formatConnections(item.id))
                    .flatMap(List::stream)
                    .map(String::trim)
                    .filter(not(String::isEmpty))
                    .collect(Collectors.toList());
        }
        return null;
    }

    private Function<Connection, List<String>> formatConnections(String id) {
        return (conn) -> {
                    List<String> result = new LinkedList<>();

                    if (indexes.contains(id) && indexes.contains(conn.target)) {
                        if (conn.alsoViewed)
                            result.add(String.format("%s,%s,%s", id, conn.target, "ALSO_VIEWED"));
                        if (conn.alsoBought)
                            result.add(String.format("%s,%s,%s", id, conn.target, "ALSO_BOUGHT"));
                        if (conn.boughtTogether)
                            result.add(String.format("%s,%s,%s", id, conn.target, "BOUGHT_TOGETHER"));
                        if (conn.buyAfterViewing)
                            result.add(String.format("%s,%s,%s", id, conn.target, "BUY_AFTER_VIEWING"));
                    }

                    return result;
        };
    }

    private <T> Predicate<T> not(Predicate<T> predicate) {
        return predicate.negate();
    }
}
