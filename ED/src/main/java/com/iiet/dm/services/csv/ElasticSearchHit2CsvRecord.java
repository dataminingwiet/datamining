package com.iiet.dm.services.csv;

import org.elasticsearch.search.SearchHit;

import java.util.*;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class ElasticSearchHit2CsvRecord {
    private static final char QUOTE = '"';
    private static final String
            HEADER = "ID,unixReviewTime,sentiment,overall,summary,reviewerID,reviewerName,helpful,reviewText,reviewLength,reviewTime";


    public String getResultCsvHeader() {
        return HEADER;
    }

    /**
     * Translates ElasticSearchHit to String CSV Record
     *
     * @param hit search hit to be translated
     * @return csv translation result
     */
    public String translate(SearchHit hit) {
        if (Objects.nonNull(hit)) {
            return buildLine(hit.getSourceAsMap());
        } else {
            return "";
        }
    }

    private String buildLine(Map<String, Object> sourceAsMap) {

        sourceAsMap.getOrDefault("asin", "").toString();
        String reviewText = sourceAsMap.getOrDefault("reviewText", "").toString();
        return new StringJoiner(",")
                .add(asStr(sourceAsMap.getOrDefault("asin", "").toString()))
                .add(sourceAsMap.getOrDefault("unixReviewTime", "").toString())
                .add(sourceAsMap.getOrDefault("sentiment", "").toString())
                .add(sourceAsMap.getOrDefault("overall", "").toString())
                .add(asStr(sourceAsMap.getOrDefault("summary", "").toString()))
                .add(sourceAsMap.getOrDefault("reviewerID", "").toString())
                .add(asStr(sourceAsMap.getOrDefault("reviewerName", "").toString()))
                .add(average((List<Integer>) sourceAsMap.getOrDefault("helpful", Collections.emptyList())))
                .add(asStr(reviewText))
                .add(String.valueOf(reviewText.length()))
                .add(asStr(sourceAsMap.getOrDefault("reviewTime", "").toString()))
                .toString();
    }

    private CharSequence asStr(String reviewText) {
        return QUOTE + escapeString(reviewText) + QUOTE;
    }

    private String escapeString(String string) {
        return string.trim()
                .replaceAll("\"", "'")
                .replaceAll("\n", "\\n");
    }

    private String average(List<Integer> helpful) {
        return String.valueOf(helpful.stream().mapToInt(i -> i).average().getAsDouble());
    }
}
