package com.iiet.dm.services.metadata.pojo;

public class Connection {
    public String target;
    public boolean alsoBought;
    public boolean alsoViewed;
    public boolean boughtTogether;
    public boolean buyAfterViewing;

    public Connection(String target) {
        this.target = target;
    }

    @Override
    public int hashCode() {
        return target.hashCode();
    }

    public boolean equals(Object o) {
        return this.target.equals(o);
    }
}
