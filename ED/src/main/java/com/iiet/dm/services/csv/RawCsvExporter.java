package com.iiet.dm.services.csv;

import com.iiet.dm.services.utils.PrintWriterExporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Raw Csv exporter.
 */
public class RawCsvExporter extends PrintWriterExporter {

    private final PrintWriter resultCsvPW;
    private final AtomicInteger counter = new AtomicInteger(0);
    private final int FLUSH_RATE = 1000;
    private final int LOG_RATE = 2000;
    private static String RESULT_CSV_FILE_NAME = "indexed.csv";


    /**
     * Default constructor.
     */
    public RawCsvExporter(String header) throws FileNotFoundException {
        this(RESULT_CSV_FILE_NAME, header);
    }

    /**
     * Parametrized constructor
     *
     * @param fileName filename of output
     */
    public RawCsvExporter(String fileName, String header) throws FileNotFoundException {
        resultCsvPW = new PrintWriter(new FileOutputStream(new File(fileName)));
        resultCsvPW.println(header);
    }

    /**
     * Exports json to file
     *
     * @param line json to be exported
     */
    public void export(String line) {
        if (!line.isEmpty()) {
            resultCsvPW.println(line);
            if (counter.getAndIncrement() % FLUSH_RATE == 0) {
                resultCsvPW.flush();
            }
            if (counter.get() % LOG_RATE == 0) {
                System.out.println("Csv lines exported: "  + counter.get());
            }
        }
    }

    @Override
    protected PrintWriter getPrintWriter() {
        return this.resultCsvPW;
    }
}
