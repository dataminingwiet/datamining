package com.iiet.dm.services.stats.nodes;

import com.iiet.dm.services.stats.nodes.pojo.ReviewerStats;
import org.elasticsearch.search.SearchHit;

import java.util.*;
import java.util.function.Consumer;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class NodeKnowledgeGenerator {
    private static final char QUOTE = '"';
    private static final String
            HEADER = "ReviewerID,reviewerName,HelpfulAvg,LengthAvg,SentimentAvg,ReviewsCount";

    private Map<String, ReviewerStats> stats = new HashMap<>();

    public String getResultCsvHeader() {
        return HEADER;
    }

    /**
     * Translates ElasticSearchHit to String CSV Record
     *
     * @param hit search hit to be translated
     * @return csv translation result
     */
    public void consume(SearchHit hit) {
        if (Objects.nonNull(hit)) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            String reviewerId = sourceAsMap.getOrDefault("reviewerID", "").toString();
            ReviewerStats statItem = stats.get(reviewerId);
            if (statItem == null) {
                statItem = new ReviewerStats();
                statItem.setReviewerName(sourceAsMap.getOrDefault("reviewerName", "").toString());
                stats.put(reviewerId, statItem);
            }

            String reviewText = sourceAsMap.getOrDefault("reviewText", "").toString();
            int sentiment = (int) sourceAsMap.getOrDefault("sentiment", Integer.MIN_VALUE);
            double helpful
                    = average((List<Integer>) sourceAsMap.getOrDefault("helpful", Collections.emptyList()));

            statItem.setReviewCount(statItem.getReviewCount() + 1);
            statItem.setHelpfulnessTotal(statItem.getHelpfulnessTotal() + helpful);
            statItem.setLengthTotal(statItem.getLengthTotal() + reviewText.length());
            statItem.setSentimentTotal(statItem.getSentimentTotal() + sentiment);
        }
    }


    private String buildLine(Map.Entry<String, ReviewerStats> reviewerStats) {
        ReviewerStats stats = reviewerStats.getValue();

        return new StringJoiner(",")
                .add(reviewerStats.getKey())
                .add(asStr(stats.getReviewerName()))
                .add(String.valueOf(stats.getHelpfulnessTotal()/stats.getReviewCount()))
                .add(String.valueOf(stats.getLengthTotal()/stats.getReviewCount()))
                .add(String.valueOf(stats.getSentimentTotal()/stats.getReviewCount()))
                .add(String.valueOf(stats.getReviewCount()))
                .toString();
    }

    private String asStr(String reviewText) {
        return QUOTE + escapeString(reviewText) + QUOTE;
    }

    private String escapeString(String string) {
        return string.trim()
                .replaceAll("\"", "'")
                .replaceAll("\n", "\\n");
    }

    private double average(List<Integer> helpful) {
        return helpful.stream().mapToInt(i -> i).average().getAsDouble();
    }

    public void sortedByCount(Consumer<String> exporter) {
        stats.entrySet().stream().sorted(compareByCount()).map(this::buildLine).forEach(exporter::accept);
    }

    private Comparator<? super Map.Entry<String,ReviewerStats>> compareByCount() {
        return (a, b) -> (int) Math.signum(a.getValue().getReviewCount() - b.getValue().getReviewCount());
    }
}
