package com.iiet.dm.services.downloader;

import org.elasticsearch.action.search.*;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;


/**
 * Connects to ElasticSearch and
 */
public class QuerableElasticSearchDownloader {

    private static final QueryBuilder DEFAULT_QUERY = QueryBuilders.existsQuery("sentiment");
    private static final int LOG_RATE = 10000;
    private final String hostName;
    private final int port;
    private AtomicInteger counter = new AtomicInteger(0);
    private TransportClient client;

    /**
     * Default constructor.
     */
    public QuerableElasticSearchDownloader() {
        this("192.168.0.20", 9300);
    }

    /**
     * Constructor allowing to set query.
     *
     * @param host host of elasticsearch instance
     * @param port port of elasticsearch instance
     */
    public QuerableElasticSearchDownloader(String host, int port) {
        this.hostName = host;
        this.port = port;
    }

    public void init() throws UnknownHostException {
        this.client = new PreBuiltTransportClient(Settings.EMPTY)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.hostName), this.port));
    }

    public void tearDown() throws UnknownHostException {
        this.client.close();
    }

    public void download(QueryBuilder query, Consumer<SearchHit> searchHitConsumer) {
        if (Objects.isNull(searchHitConsumer)) {
            return;
        }

        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
        SearchRequest searchRequest = new SearchRequest("amazon");
        searchRequest.scroll(scroll);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query == null ? DEFAULT_QUERY : query);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest).actionGet();
        String scrollId = searchResponse.getScrollId();
        SearchHit[] searchHits = searchResponse.getHits().getHits();

        while (searchHits != null && searchHits.length > 0) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            searchResponse = client.searchScroll(scrollRequest).actionGet();
            scrollId = searchResponse.getScrollId();
            searchHits = searchResponse.getHits().getHits();

            for (SearchHit hit : searchHits) {
                searchHitConsumer.accept(hit);
                if (counter.incrementAndGet() % LOG_RATE == 0) {
                    System.out.println("Found total: " + counter.get());
                }
            }

        }
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest).actionGet();
        boolean succeeded = clearScrollResponse.isSucceeded();

    }
}
