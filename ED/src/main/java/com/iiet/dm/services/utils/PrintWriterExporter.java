package com.iiet.dm.services.utils;

import java.io.PrintWriter;
import java.util.Objects;

public abstract  class PrintWriterExporter {

    /**
     * Finalizing method
     */
    public void flushAndClose() {
        PrintWriter pw = this.getPrintWriter();
        if (Objects.nonNull(pw)) {
            pw.flush();
            pw.close();
        }
    }

    /**
     * Accessor of printwriter to be flushed and closed
     *
     * @return print writer
     */
    protected abstract PrintWriter getPrintWriter();
}
