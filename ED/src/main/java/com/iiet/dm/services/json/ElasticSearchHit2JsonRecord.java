package com.iiet.dm.services.json;

import org.elasticsearch.search.SearchHit;

import java.util.Objects;

/**
 * Transformer from ElasticSearchResult to Json Record.
 */
public class ElasticSearchHit2JsonRecord {
    /**
     * Translates ElasticSearchHit to String Json Record
     *
     * @param hit search hit to be translated
     * @return Json translation result
     */
    public String translate(SearchHit hit) {
        if (Objects.nonNull(hit)) {
            return hit.getSourceAsString();
        } else {
            return "";
        }
    }
}
