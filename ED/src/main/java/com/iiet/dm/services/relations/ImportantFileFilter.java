package com.iiet.dm.services.relations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

public class ImportantFileFilter {

    private static AtomicLong COUNTER = new AtomicLong(0);

    private static final String BOOK_FILE_NAME = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";

    public static void main(String[] args) throws IOException {

        Set<String> importantIds = new HashSet<>();
        importantIds.addAll(new IdSetLoader().getIndexes());
        PrintWriter resultPW = new PrintWriter(new FileOutputStream(new File("important_books_metadata.json")));

        Files.newBufferedReader(Paths.get(BOOK_FILE_NAME)).lines()
                .filter(isImportant(importantIds)).forEach(resultPW::println);


    }

    private static Predicate<? super String> isImportant(Set<String> importantIds) {
        return (str) -> {
            if (COUNTER.getAndIncrement() % 20000 == 0) {
                System.out.println(COUNTER.get());
            }
            return importantIds.contains(str.substring(10, 20));
        };
    }
}
