package com.iiet.dm.services.relations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class IdSetLoader
{
    private static final String INDEXES_FILE_NAME = "indexes.txt";

    public List<String> getIndexes() throws IOException {
        return Files.readAllLines(Paths.get(INDEXES_FILE_NAME));
    }
}
