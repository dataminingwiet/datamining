package com.iiet.dm.services.stats.edges;

import com.iiet.dm.services.metadata.pojo.Connection;
import com.iiet.dm.services.metadata.pojo.Item;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class ProductsRankEdgesGenerator {
    private static final char QUOTE = '"';
    private static final String
            HEADER = "Worse,Better";
    
    private static Set<String> handledIndexes = new HashSet<>();

    public String getResultCsvHeader() {
        return HEADER;
    }

    /**
     * Translates Metadata Item to String CSV Record
     *
     * @param item metadata item to be transformed
     * @return csv translation result
     */
    public List<String> consume(Item item) {
        if (!handledIndexes.contains(item.id)) {
            handledIndexes.add(item.id);
            long buyAfterViewing = getLength(item, con -> con.buyAfterViewing);
            if (buyAfterViewing > 0) {
                return item.related.stream()
                        .filter(con -> con.buyAfterViewing)
                        .map(better -> item.id + "," + better.target)
                        .collect(Collectors.toList());
            }
        } else {
            System.out.println("Duplicate found!");
        }
        return Collections.emptyList();
    }

    private long getLength(Item item, Predicate<Connection> fn) {
        return item.related.stream().filter(fn).count();
    }
}
