package com.iiet.dm.services.categories;

import com.iiet.dm.services.metadata.pojo.Connection;
import com.iiet.dm.services.metadata.pojo.Item;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.awt.SystemColor.info;

/**
 * Transformer from ElasticSearchResult to CSV Record.
 */
public class CategoriesRankGenerator {
    private final static int LOG_RATE = 10000;
    private final AtomicLong LOG_COUNTER = new AtomicLong(0);

    private static final String SEPARATOR = "::";
    private static final String HEADER_EDGE = "Source,Target,Strength";
    private static final String HEADER_NODE = "ID,Level,Power,PowerOverLevel,InversedLevel";

    private static Map<String, Set<Integer>> itemCategories = new HashMap<>();
    private static Map<String, CategoryInfo> allCategoriesByName = new HashMap<>();
    private static Map<Integer, CategoryDetails> allCategoriesById = new HashMap<>();
    private static Map<String, Integer> edgesPower = new HashMap<>();

    class CategoryInfo {
        public int level;
        public int power;
        public int id;
    }

    class CategoryDetails {
        public int level;
        public int power;
        public String name;
    }

    public String getNodeCsvHeader() {
        return HEADER_NODE;
    }

    public String getEdgeCsvHeader() {
        return HEADER_EDGE;
    }

    /**
     * Builds product-multiLevelCategories map
     *
     * @param item metadata item to be transformed
     */
    public void buildMapOfCategories(Item item) {
        if (item.categories != null) {
            Set<Integer> categoriesSet = new HashSet<>();
            itemCategories.put(item.id, categoriesSet);
            item.categories.stream()
                    .filter(Objects::nonNull)
                    .forEach(addMultiLayerCategory(categoriesSet));

            handleLogging("MAP OF CATEGORIES");
        }
    }

    private void handleLogging(String stepName) {
        long value = LOG_COUNTER.incrementAndGet();
        if (value % LOG_RATE == 0) {
            System.out.println(stepName + "> Items handled: " + LOG_COUNTER.get());
        }
    }

    private Consumer<List<String>> addMultiLayerCategory(Set<Integer> categories) {
        return list -> {
            StringJoiner sj = new StringJoiner(SEPARATOR);
            IntStream.range(0, list.size())
                    .forEach(level -> {
                        sj.add(list.get(level));
                        String current = sj.toString();

                        CategoryInfo retrievedInfo = allCategoriesByName.get(current);
                        if (retrievedInfo == null) {
                            retrievedInfo = new CategoryInfo();
                            retrievedInfo.id = allCategoriesByName.size();
                            retrievedInfo.level = level;
                            retrievedInfo.power = 0;
                            allCategoriesByName.put(current, retrievedInfo);
                        }
                        if (!categories.contains(retrievedInfo.id)){
                            retrievedInfo.power +=1;
                            categories.add(retrievedInfo.id);
                        }
                    });
        };
    }

    public void prepareForStep2() {
        allCategoriesByName.entrySet().stream().forEach(
                name2info -> {
                    CategoryDetails details = new CategoryDetails();
                    details.level = name2info.getValue().level;
                    details.name = name2info.getKey();
                    details.power = name2info.getValue().power;
                    allCategoriesById.put(name2info.getValue().id, details);
                }
        );
        allCategoriesByName.clear();
        System.gc();
    }


    /**
     * Builds multiLevelCategory-multiLevelCategory weighted map
     *
     * @param item metadata item to be transformed
     */
    public void buildRelations(Item item) {
        if (item != null && item.related != null) {
            Set<Integer> sourceCategories = itemCategories.get(item.id);
            if (sourceCategories == null || sourceCategories.isEmpty()) {
                return;
            }
            item.related.stream()
                    .filter(con -> con.buyAfterViewing)
                    .forEach(connection -> {

                        Set<Integer> targetCategories = itemCategories.get(connection.target);

                        if (targetCategories != null) {
                            sourceCategories.stream().forEach(from -> {

                                        String fromKey = allCategoriesById.get(from).name;

                                        targetCategories.stream().forEach(to -> {

                                            String key = fromKey + "," + allCategoriesById.get(to).name;

                                            Integer value = edgesPower.get(key);
                                            if (value == null) {
                                                value = 1;
                                            } else {
                                                value += 1;
                                            }

                                            edgesPower.put(key, value);
                                        });

                                    }
                            );
                        }

                    });
            handleLogging("RELATIONS BUILDING");
        }
    }

    public void flushSupportiveInfo() {
        itemCategories.clear();
    }

    public Stream<String> getNodesAsStream() {
        return allCategoriesById.entrySet().stream()
                .map(entry ->
                        entry.getValue().name + "," +
                        entry.getValue().level + ", " +
                        entry.getValue().power + ", " +
                        (1.0 * entry.getValue().power/ (entry.getValue().level + 1)) + ", " +
                        (10 - entry.getValue().level));
    }

    public Stream<String> getEdgesAsStream() {
        return edgesPower.entrySet().stream()
                .map(entry -> entry.getKey() + "," + entry.getValue());
    }
}
