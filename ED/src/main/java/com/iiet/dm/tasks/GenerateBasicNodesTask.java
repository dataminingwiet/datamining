package com.iiet.dm.tasks;

import com.iiet.dm.services.csv.ElasticSearchHit2CsvRecord;
import com.iiet.dm.services.csv.RawCsvExporter;
import com.iiet.dm.services.downloader.ElasticSearchDownloader;
import com.iiet.dm.services.indexes.IndexesConsumer;
import com.iiet.dm.services.indexes.IndexesExporter;
import com.iiet.dm.services.json.ElasticSearchHit2JsonRecord;
import com.iiet.dm.services.json.RawJsonExporter;
import org.elasticsearch.search.SearchHit;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Downloads reviews from ElasticSearch and generates output files
 */
public class GenerateBasicNodesTask {

    // consumers
    private ElasticSearchHit2CsvRecord es2csv;
    private ElasticSearchHit2JsonRecord esjson;
    private IndexesConsumer indexConsumer;

    // exporters
    private RawCsvExporter csvExporter;
    private RawJsonExporter jsonExporter;
    private IndexesExporter indexesExporter;

    public GenerateBasicNodesTask() throws IOException{
        es2csv = new ElasticSearchHit2CsvRecord();
        esjson = new ElasticSearchHit2JsonRecord();
        indexConsumer = new IndexesConsumer();

        // exporters
        csvExporter = new RawCsvExporter(es2csv.getResultCsvHeader());
        jsonExporter = new RawJsonExporter();
        indexesExporter = new IndexesExporter();
    }


    /**
     * Generator execution method
     */
    public void run() throws IOException {
        ElasticSearchDownloader downloader = new ElasticSearchDownloader();
        downloader.download(getComplexConsumer());
        csvExporter.flushAndClose();
        jsonExporter.flushAndClose();
        indexesExporter.export(indexConsumer.getIndexes()).flushAndClose();
    }

    private Consumer<SearchHit> getComplexConsumer() throws IOException {

        return (hit) -> {
            csvExporter.export(es2csv.translate(hit));
            jsonExporter.export(esjson.translate(hit));
            indexConsumer.accept(hit);
        };
    }
}
