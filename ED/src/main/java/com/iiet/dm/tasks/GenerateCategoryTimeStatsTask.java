package com.iiet.dm.tasks;

import com.iiet.dm.services.categories.CategoriesRankGenerator;
import com.iiet.dm.services.csv.RawCsvExporter;
import com.iiet.dm.services.downloader.ElasticSearchDownloader;
import com.iiet.dm.services.metadata.parser.Parser;
import com.iiet.dm.services.stats.simple.CategoryMonthGenerator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class GenerateCategoryTimeStatsTask {

    private final static String BOOKS_METADATA = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";

    // consumers
    private Parser metadataParser;
    private CategoryMonthGenerator knowledgeGenerator;
    // exporters
    private RawCsvExporter statsCsvExporter;

    public GenerateCategoryTimeStatsTask() throws FileNotFoundException {
        metadataParser = new Parser();
        knowledgeGenerator = new CategoryMonthGenerator();
        statsCsvExporter = new RawCsvExporter("stats.csv", knowledgeGenerator.getResultCsvHeader());
    }

    public void run() throws IOException {
        Files.lines(Paths.get(BOOKS_METADATA))
                .map(metadataParser::parseItem)
                .filter(Objects::nonNull)
                .forEach(knowledgeGenerator::buildMapOfCategories);

        ElasticSearchDownloader downloader = new ElasticSearchDownloader();
        downloader.download(knowledgeGenerator::buildResultObject);
        knowledgeGenerator.getResultsAsStream().forEach(statsCsvExporter::export);
        statsCsvExporter.flushAndClose();
    }
}
