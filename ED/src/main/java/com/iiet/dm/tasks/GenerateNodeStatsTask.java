package com.iiet.dm.tasks;

import com.iiet.dm.services.csv.RawCsvExporter;
import com.iiet.dm.services.downloader.ElasticSearchDownloader;
import com.iiet.dm.services.stats.nodes.NodeKnowledgeGenerator;

import java.io.IOException;

/**
 * Downloads reviews from ElasticSearch and generates output files
 */
public class GenerateNodeStatsTask {

    // consumers
    private NodeKnowledgeGenerator knowledgeGenerator;
    // exporters
    private RawCsvExporter csvExporter;

    public GenerateNodeStatsTask() throws IOException{
        // exporters
        knowledgeGenerator = new NodeKnowledgeGenerator();
        csvExporter = new RawCsvExporter("reviewers_by_count.csv",knowledgeGenerator.getResultCsvHeader());
    }


    /**
     * Generator execution method
     */
    public void run() throws IOException {
        ElasticSearchDownloader downloader = new ElasticSearchDownloader();
        downloader.download(knowledgeGenerator::consume);
        knowledgeGenerator.sortedByCount(csvExporter::export);
        csvExporter.flushAndClose();
    }
}
