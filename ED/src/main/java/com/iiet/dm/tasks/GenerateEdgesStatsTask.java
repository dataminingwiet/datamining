package com.iiet.dm.tasks;

import com.iiet.dm.services.csv.RawCsvExporter;
import com.iiet.dm.services.metadata.parser.Parser;
import com.iiet.dm.services.stats.edges.EdgeKnowledgeGenerator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Downloads reviews from ElasticSearch and generates output files
 */
public class GenerateEdgesStatsTask {

    private final static String BOOKS_METADATA = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";
    // consumers
    private Parser metadataParser;
    private EdgeKnowledgeGenerator knowledgeGenerator;
    // exporters
    private RawCsvExporter csvExporter;

    public GenerateEdgesStatsTask() throws IOException{
        // exporters
        knowledgeGenerator = new EdgeKnowledgeGenerator();
        csvExporter = new RawCsvExporter("relation_strength_stats.csv", knowledgeGenerator.getResultCsvHeader());
        metadataParser = new Parser();
    }


    /**
     * Generator execution method
     */
    public void run() throws IOException {
        Files.lines(Paths.get(BOOKS_METADATA))
                .map(metadataParser::parseItem)
                .filter(Objects::nonNull)
                .map(knowledgeGenerator::consume)
                .filter(str -> str != null)
                .filter(str -> !str.isEmpty())
                .forEach(csvExporter::export);
        csvExporter.flushAndClose();
    }
}
