package com.iiet.dm.tasks;

import com.iiet.dm.services.analysis.export.SentimentAnalysisDataExporter;
import com.iiet.dm.services.analysis.first.FirstClassifierDataLoader;
import com.iiet.dm.services.analysis.pojo.AnalysisResult;
import com.iiet.dm.services.analysis.second.SecondClassifierDataDecorator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class GenerateSentimentAnalysisData {

    private FirstClassifierDataLoader firstClassifierDataLoader;
    private SecondClassifierDataDecorator secondClassifierDataDecorator;
    private SentimentAnalysisDataExporter sentimentAnalysisDataExporter;

    public GenerateSentimentAnalysisData() throws FileNotFoundException {
        this.firstClassifierDataLoader = new FirstClassifierDataLoader();
        this.secondClassifierDataDecorator = new SecondClassifierDataDecorator();
        this.sentimentAnalysisDataExporter = new SentimentAnalysisDataExporter();
    }

    public void run() {
        try {
            List<AnalysisResult> results = firstClassifierDataLoader.readResultsOfFirstClassifier();
            secondClassifierDataDecorator.decorate(results);
            sentimentAnalysisDataExporter.export(results);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
