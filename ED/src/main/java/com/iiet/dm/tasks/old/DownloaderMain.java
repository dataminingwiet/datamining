package com.iiet.dm.tasks.old;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.*;

public class DownloaderMain {
    private static PrintWriter resultPW;
    private static String RESULT_FILE_NAME = "indexed.csv";

    private static String HEADER = "ID,unixReviewTime,sentiment,overall,summary,reviewerID,reviewerName,helpful,reviewText,reviewTime";
    private static Set<String> idHolder = new HashSet<>();

    public static void main(String[] args) throws FileNotFoundException {
////        resultPW = old PrintWriter(old FileOutputStream(old File(RESULT_FILE_NAME)));
////        indexesPW = old PrintWriter(old FileOutputStream(old File(INDEXES_FILE_NAME)));
//        resultJsonPW = old PrintWriter(old FileOutputStream(old File(RESULT_JSON_FILE_NAME)));
//        try (TransportClient client = old PreBuiltTransportClient(Settings.EMPTY)
//                .addTransportAddress(old InetSocketTransportAddress(InetAddress.getByName("192.168.0.14"), 9300))) {
////            resultPW.println(HEADER);
//
//            int i = 0;
//            SearchResponse response = null;
//            Map<String, Object> itemMap;
//            while (response == null || response.getHits().getHits().length != 0) {
//                response = client.prepareSearch("amazon")
//                        .setQuery(QueryBuilders.existsQuery("sentiment"))
//                        .setSize(100)
//                        .setFrom(i * 100)
//                        .execute()
//                        .actionGet();
//
//                for (SearchHit hit : response.getHits()) {
//                    itemMap = hit.getSourceAsMap();
//                    idHolder.add(itemMap.getOrDefault("asin", "").toString());
//                    String s = buildLine(itemMap);
//                    if(!s.isEmpty()) {
////                        resultPW.println(s);
//                        resultJsonPW.println(hit.getSourceAsString());
//                    }
//
//                }
//                System.out.println(i++  * 100);
////                resultPW.flush();
//            }
//
////            idHolder.stream().forEach(indexesPW::println);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
////            resultPW.flush();
////            resultPW.close();
////            indexesPW.flush();
////            indexesPW.close();
//            resultJsonPW.flush();
//            resultJsonPW.close();
//        }
    }

    private static String buildLine(Map<String, Object> sourceAsMap) {

        sourceAsMap.getOrDefault("asin", "").toString();

        return new StringJoiner(",")
                .add(asStr(sourceAsMap.getOrDefault("asin", "").toString()))
                .add(sourceAsMap.getOrDefault("unixReviewTime", "").toString())
                .add(sourceAsMap.getOrDefault("sentiment", "").toString())
                .add(sourceAsMap.getOrDefault("overall", "").toString())
                .add(asStr(sourceAsMap.getOrDefault("summary", "").toString()))
                .add(sourceAsMap.getOrDefault("reviewerID", "").toString())
                .add(asStr(sourceAsMap.getOrDefault("reviewerName", "").toString()))
                .add(average((List<Integer>) sourceAsMap.getOrDefault("helpful", Collections.emptyList())))
                .add(asStr(sourceAsMap.getOrDefault("reviewText", "").toString()))
                .add(asStr(sourceAsMap.getOrDefault("reviewTime", "").toString()))
                .toString();
    }

    private static CharSequence asStr(String reviewText) {
        return '"' + reviewText.trim().replaceAll("\"", "'").replaceAll("\n", "\\n") + '"';
    }

    private static String average(List<Integer> helpful) {
        return String.valueOf(helpful.stream().mapToInt(i -> i).average().getAsDouble());
    }
}
