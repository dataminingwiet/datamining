package com.iiet.dm.tasks;

import com.iiet.dm.services.metadata.old.instruments.GephiCSVGenerator;

import java.io.IOException;

public class Metadata2CsvEdgeTask {
    private final static String BOOKS_METADATA = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";
    private final static String OUTPUT_FOLDER = "books";

    public static void main(String[] args) throws IOException {
        new GephiCSVGenerator().run(BOOKS_METADATA, OUTPUT_FOLDER);
    }
}
