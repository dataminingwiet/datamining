package com.iiet.dm.tasks;

import com.iiet.dm.services.csv.RawCsvExporter;
import com.iiet.dm.services.metadata.parser.Parser;
import com.iiet.dm.services.stats.edges.ProductsRankEdgesGenerator;
import com.iiet.dm.services.stats.edges.ProductsRankNodesGenerator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

/**
 * Downloads reviews from ElasticSearch and generates output files
 */
public class GenerateNodesForProductRankTask {

    private final static String BOOKS_METADATA = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";
    // consumers
    private Parser metadataParser;
    private ProductsRankNodesGenerator knowledgeGenerator;
    // exporters
    private RawCsvExporter csvExporter;

    public GenerateNodesForProductRankTask() throws IOException{
        // exporters
        knowledgeGenerator = new ProductsRankNodesGenerator();
        csvExporter = new RawCsvExporter("product_rank_nodes.csv", knowledgeGenerator.getResultCsvHeader());
        metadataParser = new Parser();
    }


    /**
     * Generator execution method
     */
    public void run() throws IOException {
        Files.lines(Paths.get(BOOKS_METADATA))
                .map(metadataParser::parseItem)
                .filter(Objects::nonNull)
                .map(knowledgeGenerator::consume)
                .filter(str -> str != null)
                .filter(str -> !str.isEmpty())
                .forEach(csvExporter::export);
        csvExporter.flushAndClose();
    }
}
