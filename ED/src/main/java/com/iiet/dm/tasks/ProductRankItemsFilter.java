package com.iiet.dm.tasks;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class ProductRankItemsFilter {

    private Set<String> supportedIndexes = new HashSet<>();

    public static void main(String[] args) throws IOException {
        new ProductRankItemsFilter().filter();
    }

    public void filter() throws IOException {
        BufferedReader nodesReader = new BufferedReader(new FileReader(new File("product_rank_nodes.csv")));
        BufferedReader edgesReader = new BufferedReader(new FileReader(new File("product_rank_edges.csv")));

        PrintWriter nodesWriter = new PrintWriter(new FileOutputStream("product_rank_nodes_filtered.csv"));
        PrintWriter edgesWriter = new PrintWriter(new FileOutputStream("product_rank_edges_filtered.csv"));

        nodesReader.lines()
                .filter(str -> !str.endsWith(",0"))
                .filter(str -> !str.contains(",\"\","))
                .peek(str -> supportedIndexes.add(str.split(",")[0]))
                .forEach(nodesWriter::println);

        edgesReader.lines()
                .map(str -> str.split(","))
                .filter(str -> supportedIndexes.contains(str[0]))
                .filter(str -> supportedIndexes.contains(str[1]))
                .map(str -> str[0] + "," + str[1])
                .forEach(edgesWriter::println);

        nodesWriter.flush();
        edgesWriter.flush();

        nodesWriter.close();
        edgesWriter.close();

        nodesReader.close();
        edgesReader.close();
    }
}
