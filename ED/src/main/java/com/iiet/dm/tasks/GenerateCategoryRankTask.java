package com.iiet.dm.tasks;

import com.iiet.dm.services.categories.CategoriesRankGenerator;
import com.iiet.dm.services.csv.RawCsvExporter;
import com.iiet.dm.services.metadata.parser.Parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Downloads reviews from ElasticSearch and generates output files
 */
public class GenerateCategoryRankTask {

    private final static String BOOKS_METADATA = "/home/robert/AGHStudia/ED/Metadata/meta_Books.json";

    // consumers
    private Parser metadataParser;
    private CategoriesRankGenerator knowledgeGenerator;
    // exporters
    private RawCsvExporter nodeCsvExporter;
    private RawCsvExporter edgeCsvExporter;

    public GenerateCategoryRankTask() throws IOException{
        // exporters
        knowledgeGenerator = new CategoriesRankGenerator();
        nodeCsvExporter = new RawCsvExporter("categories_rank_nodes.csv", knowledgeGenerator.getNodeCsvHeader());
        edgeCsvExporter = new RawCsvExporter("categories_rank_edges.csv", knowledgeGenerator.getEdgeCsvHeader());
        metadataParser = new Parser();
    }


    /**
     * Generator execution method
     */
    public void run() throws IOException {
        Files.lines(Paths.get(BOOKS_METADATA))
                .map(metadataParser::parseItem)
                .filter(Objects::nonNull)
                .forEach(knowledgeGenerator::buildMapOfCategories);
        System.out.println("Nodes generated!");

        knowledgeGenerator.prepareForStep2();

        Files.lines(Paths.get(BOOKS_METADATA))
                .map(metadataParser::parseItem)
                .filter(Objects::nonNull)
                .forEach(knowledgeGenerator::buildRelations);
        System.out.println("Edges generated!");

        knowledgeGenerator.flushSupportiveInfo();

        knowledgeGenerator.getNodesAsStream()
                .filter(str -> str != null)
                .filter(str -> !str.isEmpty())
                .forEach(nodeCsvExporter::export);
        System.out.println("Nodes exported!");

        knowledgeGenerator.getEdgesAsStream()
                .filter(str -> str != null)
                .filter(str -> !str.isEmpty())
                .forEach(edgeCsvExporter::export);
        System.out.println("Edges exported!");

    }
}
