package com.iiet.dm;

import com.iiet.dm.tasks.*;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
//        new GenerateNodeStatsTask().run();
//        new GenerateEdgesStatsTask().run();
//        new GenerateEdgesForProductRankTask().run();
//        new GenerateNodesForProductRankTask().run();
//        new GenerateCategoryRankTask().run();
//        new GenerateCategoryTimeStatsTask().run();
          new GenerateSentimentAnalysisData().run();
    }

}
