import json
import gzip
import requests
from math import floor
from datetime import datetime

# url = "http://text-processing.com/api/sentiment/"
url = "http://sentiment.vivekn.com/web/text/"
positive = 1
negative = -1
neutral = 0

mode = 'a'

years = range(2013, 1995, -1)

files = {}
for x in years:
     files[x] = open("./tagged/" + str(x) + ".json", mode)

def deal_sentiment(sent):
  if (sent == "pos" or sent == "Positive"):
    return positive
  if (sent == "neg" or sent == "Negative"):
    return negative
  return neutral

def parse(path):
  g = open(path, 'r')
  for l in g:
    review_json = eval(l)

    text = review_json['reviewText']
    params = {'txt' : text}
    r = requests.post(url, data=params)

    raw_sentiment = r.json()['result']

    sentiment = deal_sentiment(raw_sentiment)
    # print(sentiment)
    review_json["sentiment"] = sentiment

    date = review_json['unixReviewTime']
    year = datetime.fromtimestamp(date).year
    files[year].write(json.dumps(review_json) + "\n")

    #yield json.dumps(l)
    yield year

f = open("output.strict", 'w')
i = 1
for year in years:
  print "Current year " + str(year)
  i = 1
  for l in parse("./divided/" + str(year) + ".json"):
    i+=1
