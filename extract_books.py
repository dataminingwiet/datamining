import json
import gzip
import requests


url     = 'http://localhost:9200/amazon/book/'
headers = {'Content-Type': 'application/json'}

def parse(path):
  g = open(path, 'r')
  for l in g:
    yield l # json.dumps(eval(l))


f = open("output.strict", 'w')

for year in range(2013, 2015):
  i = 1
  for l in parse("./tagged/" + str(year) + ".json"): #reviews_Books_5.json.gz"):
    doc_url = url +"book_"+ str(year) + "_" + str(i)
    res = requests.put(doc_url, data=l, headers=headers)
    i+=1
