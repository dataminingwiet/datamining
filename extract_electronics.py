import json
import gzip
import requests


url     = 'http://localhost:9200/amazon/review_electronics/'
headers = {'Content-Type': 'application/json'}

def parse(path):
  g = gzip.open(path, 'r')
  for l in g:
    yield json.dumps(eval(l))


f = open("output.strict", 'w')
i = 1
for l in parse("reviews_Electronics_5.json.gz"):
  doc_url = url +"electronic"+ str(i)
  res = requests.put(doc_url, data=l, headers=headers)
  i+=1
